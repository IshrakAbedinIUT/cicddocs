# Week3 Report: Automating Service Creation, Virtualization and Docker
------
## Goal

In the past week, we have automatically deployed our dotnet core web api using a Jenkins Multi-branch pipeline as a SystemD daemon service. However, the creation of the service file, *netcoreapi.service* or *netcoreapidev.service* was manual in the app or deployment server. We need to automate this part to prevent further manual interventions and automatically handle the service creation or restart depending on the novelty of the deployed application. We will be using an application called *jq* and *json* files to automatically create or restart the daemon service.

------
## Related Information

### JavaScript Object Notation (JSON)

JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. It is based on a subset of the JavaScript Programming Language Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is completely language independent but uses conventions that are familiar to programmers of the C-family of languages, including C, C++, C#, Java, JavaScript, Perl, Python, and many others. These properties make JSON an ideal data-interchange language. More about JSON and its format can be found at the [official website](https://www.json.org/json-en.html).

### JQ

Jq is a tool designed to retrieve and manipulate JSON data. It helps in reading values of keys inside JSON files easily while using bash scripts. Jq is written in *portable c* and so, it has zero dependencies and can work standalone on any linux system. More about Jq can be found at their [official webpage](https://stedolan.github.io/jq/).

### Linux Virtualization

Virtualization is a technique of running multiple operating systems using the same hardware. A special type of software, called *Hypervisor* is used to perform virtualization. The system on which the virtualized systems are running is called the **Host** system and the the virtualized systems are called **Guest** systems. The guest systems share the host system's hardware and resources. When the virtual systems virtualize even the hardware and act like independent machines, they are called **virtual machines**. The classification of virtualization is quite ambiguous and varies from source to source. According to [Opensource.com](https://opensource.com/resources/virtualization), there are two main types of virtualization.

1. **Bare-metal virtualization:** In bare-metal virtualization, the hypervisor is directly mounted on the hardware. It acts like a special bare-bone OS, the main purpose of which is to virtualize other operation systems. *KVM*, *ESX* etc. are examples of bare-metal hypervisors. KVM has both bare-metal and hosted version.

2. **Hosted virtualization:** In hosted virtualization, the hypervisor software runs on a host operating system. The guest operating systems run on top the host operating system. Commonly used *VMWare*, *Oracle Virtual Box*, *KVM* etc. are examples of hosted hypervisors.

Again, according to [Wikipedia](https://en.wikipedia.org/wiki/Virtualization), virtualization can be categorized into two main categories.

1. **Hardware Virtualization:** In hardware virtualization, all the necessary hardwares of the guest operating systems are virtualized. Hardware virtualization can be classified into two main types based on their co-operation.

    + *Full Virtualization:* In full virtualization, the hardwares are almost completely virtualized and guest operating systems can perform on their own while completely being isolated.
    
    + *Para-virtualization:* In para-virtualization technique, full hardware is not virtualized. The guest operating systems remain aware of each other and co-operate in order to maintain hardware and resource distribution. Though it offers better performance, softwares need to be specially modified and the host and guest operating systems need to implement same kernels.

2. **Desktop Virtualization:** Desktop virtualization logically separates desktops from the hardwares. Rather than interacting with a host computer directly via a keyboard, mouse, and monitor, the user interacts with the host computer using another desktop computer or a mobile device by means of a network connection, such as a LAN, Wireless LAN or even the Internet. In addition, the host computer in this scenario becomes a server computer capable of hosting multiple virtual machines at the same time for multiple users.

### Linux Containers

Linux containers are conceptually similar to virtual machines, but function somewhat differently. While both containers and virtual machines allow for running applications in an isolated environment, allowing a user to stack many onto the same machine as if they are separate computers, containers are not full, independent machines. A container is actually just an isolated process that shared the same Linux kernel as the host operating system, as well as the libraries and other files needed for the execution of the program running inside of the container, often with a network interface such that the container can be exposed to the world in the same way as a virtual machine. Typically, containers are designed to run a single program, as opposed to emulating a full multi-purpose server. More about Linux containers can be found at their [official website](https://linuxcontainers.org/) and [Wikipedia](https://en.wikipedia.org/wiki/LXC).

### Docker

Docker is a set of *platform as a service (PaaS)* products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files. They can communicate with each other through well-defined channels. All containers are run by a single operating-system kernel and are thus more lightweight than virtual machines. Thus containers help to remove dependencies of an application to make it universally deployable on any machine that runs *Docker Engine*, an engine to run *Docker Images*. A running *Docker Image* becomes a *Docker Container*. More about Docker can be found at their [official website](https://www.docker.com/) and [Wikipedia](https://en.wikipedia.org/wiki/Docker_(software)).

------
## Tasks
![Week 3 Tasks](./resources/week3tasks.png "Week3 Tasks")

------
## Task 1: Automate service file generation

We will use a special script that resides on the application server to automate daemon service handling. If the service exists, it will automatically restart it. But in the case it does not exist, the script will automatically create it, register it in *systemctl* and then enable and start the service. In order to provide a recipe for the service file, we will pass a *JSON* file, in our case called *recipe.json* from build server to application server using *Jenkins*, *ssh* and *rsync* which the script will take as parameter for service file generation or restarting. A sample *JSON* file for the master branch called *recipe.json* is given below.
```json
{
"ServiceName": "netcoreapi.service",
"Description":"netcoreapi",
"PkgPath":"/home/ishrak/NC2/dotnet",
"ServicePath":"/home/ishrak/app/netcoreapi/netcore-api.dll",
"ServiceURL":"http://localhost:5000/"
} 
```

A sample *Bash* script called *createsvc* is given below.

```bash
#!/usr/bin/bash
# Store the recipe.json file path passed as the first argument
JsonPath=$1

# Path for daemon services in system
DaemonPath="/etc/systemd/system/"

# Read the values from recipe.json file
ServiceName=$(jq .ServiceName $JsonPath -r)
Description=$(jq .Description $JsonPath -r)
PkgPath=$(jq .PkgPath $JsonPath -r)
ServicePath=$(jq .ServicePath $JsonPath -r)
ServiceURL=$(jq .ServiceURL $JsonPath -r)

# Complete service path
SDPath=$DaemonPath$ServiceName

# Just restart the service if it already exists
if [ -e $SDPath ]; then
echo "$ServiceName service already exists."
echo "Restarting $ServiceName"
sudo systemctl restart $ServiceName

# Create, register and enable and start service if it does not exist
else
echo "$ServiceName service does not exist."
echo "Making new service $ServiceName."
touch $SDPath
cat > $SDPath <<EOF
[Unit]
Description=$Description service
After=network.target

[Service]
ExecStart=$PkgPath $ServicePath --urls=$ServiceURL
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable $ServiceName
sudo systemctl start $ServiceName
fi
```

An equivalent *Python* script is like below.

```python
import json
import subprocess
from os import path
from sys import argv

OUTPUT_LOCATION = "/etc/systemd/system/"

def main():
    data = {}
    jsonPath = getJsonPath()
    with open(jsonPath) as jsonFile:
        data = json.load(jsonFile)
    serviceName = data["ServiceName"]
    filePath = OUTPUT_LOCATION + serviceName
    
    if(path.exists(filePath)):
        print("Daemon already exists, restarting.")
        daemonRestart(serviceName)

    else:
        print("No existing daemon found. Trying to make a new one")
        unitData = getUnitTemplate().format(data["Description"], data["PkgPath"], data["ServicePath"], data["ServiceURL"])
        with open(filePath, "w") as serviceFile:
            serviceFile.write(unitData)
        daemonActive(serviceName)
    
    return

def getUnitTemplate():
    unitTemplate = "[Unit]\n"
    unitTemplate += "Description={0} service\n"
    unitTemplate += "After=network.target\n\n"
    unitTemplate += "[Service]\n"
    unitTemplate += "ExecStart={1} {2} --urls={3}\n"
    unitTemplate += "Restart=on-failure\n\n"
    unitTemplate += "[Install]\n"
    unitTemplate += "WantedBy=multi-user.target\n"
    return unitTemplate

def daemonActive(serviceName):
    subprocess.run("sudo systemctl daemon-reload", shell=True, check=True)
    subprocess.run("sudo systemctl enable {0}".format(serviceName), shell=True, check=True)
    subprocess.run("sudo systemctl start {0}".format(serviceName), shell=True, check=True)

def daemonRestart(serviceName):
    subprocess.run("sudo systemctl restart {0}".format(serviceName), shell=True, check=True)

def getJsonPath():
    if(len(argv) == 2):
        filePath = argv[1]
        return filePath
    else:
        print("Please pass the path of recipe as the only argument.")
        exit(1)

main()
```

## Task 2: Modify Jenkinsfile to use service generation automation

Now we just need to add a *recipe.json* file to our project and gradually pass it to application server and then run the *automation script* with this *recipe.json* as argument. We can all do this easily using *Jenkins* by modifying the *Jenkinsfile*. A sample *Jenkinsfile* is given below for the master branch.

```groovy
pipeline{
    agent any
    stages{
        stage('Master Build'){
            when {
                branch 'master' 
            }
            steps{
                sh "/home/ishrak/NC2/dotnet restore"
                sh "/home/ishrak/NC2/dotnet clean"
                sh "/home/ishrak/NC2/dotnet build"
                sh "/home/ishrak/NC2/dotnet publish -c release -o ./Output"
            }
        }

        stage('Dev Test'){
            when {
                branch 'dev' 
            }
            steps{
                sh "/home/ishrak/NC2/dotnet restore"
                sh "/home/ishrak/NC2/dotnet clean"
                sh "/home/ishrak/NC2/dotnet build"
                sh "/home/ishrak/NC2/dotnet test --logger 'trx;LogFileName=NetCore.API.trx' -r ./Result/ netcore-api.sln"
            }
        }

        stage('Master Deploy'){
            when {
                branch 'master' 
            }
            steps{
                sh "rsync -avz ./api/Output/ ishrak@localhost:app/netcoreapi/"
                sh "rsync -avz ./recipe.json ishrak@localhost:app/netcoreapi/"
            }
        }

        stage('Master Daemon Restart'){
            when {
                branch 'master' 
            }
            steps{
                sh 'ssh -v ishrak@localhost "sudo /home/ishrak/daemontools/createsvc.sh /home/ishrak/app/netcoreapi/recipe.json"'
            }
        }
    }
}
```

## Task 3: Testing the project
If all the steps were performed properly, we will be able to see the status of our application properly by typing, 

```bash
$ sudo systemctl status demoapp.service
```
It will show it is enabled, active and loaded.

We can now go to a browser or use *curl* and dial *http://localhost:5000/api/values* to get a sample result, current date and time, to check whether the application is working properly or not.

## Task 4: Studying about Linux virtualization and Docker

The topics are already discussed in brief in the beginning. It is highly recommended to visit the websites and read about the topics and their official documentations.

------
## Reading Topics
------
- Linux Virtualization
- Docker
------
## Related
------
1. [Sample DotNet project with Jenkins file](https://bitbucket.org/IshrakAbedinIUT/netcore-api/)