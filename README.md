# A documentation about CI/CD using Jenkins
This documentation serves as a note for the process followed in developing a sample CI/CD system using Git, BitBucket and Jenkins. The report for each week can be found under week[number] folders along with the used resources. The week[number].md files contain the reports.
## What are Agile process, CI/CD and DevOps?
With the advancement of time, requirements of software development have changed and evolved a lot. Before, only few people used to use computer programmes and their usage were confined to very few specific cases. So, a development team could take months or even years to build and release a software for a company or a specific purpose and generally, there was not much post release works. But in the current days, demands for software have changed a lot. People, in their everyday life are using a huge number of softwares in mobile, desktop or even integrated and embedded forms. And the demand is ever increasing. People now can remove and install softwares on the go if they do not like one for a specific purpose since there are mounds to choose from. People want three things from modern softwares.

1. Newer Features
2. Lesser (preferably none at all) bugs
3. All these things delivered as soon as and as frequently as possible

Traditional software development models fail to cope up with these requirement. This is where agile process model of software development comes into play.

## Agile Process Model
The concept of Agile is pretty simple. The development team needs to be agile in every process. In agile process, the developers and the customers work together. To err is human and it is natural that the development team will not understand all the requirements in the beginning. At the same time, the customers might not be able to express all the needs in the beginning and requirements may change with time. The development team should always be open to changes as long as it is viable to achieve. The product should be delivered in increments where each cycle adds more features or performs bug fixing. The team provides continuos support for bug fixing.

## Continuous Integration/ Continuous Delivery (CI/CD)
![CI/CD Stages](./week1/resources/cicdstages.png "CI/CD Stages")
CI/CD is an agile practice. There are two parts to it, continuous integration and then continuous delivery. Every team consists of a number of members and they work in parallel. In simple word, continuous integration means all of their codes will be collected, integrated and merged in a single local place may it be local or remote. The collection and integration process needs to be automated. Example of tools for carrying out CI included Git, Mercurial for local management and GitHub, BitBucket etc for remote management.

Continuous delivery focuses on automating post integration jobs, such as building, testing, reporting and deployment on specific server(s). These actions are carried out using build server, development, staging, test and production server with the help of web server management systems such as Apache Tomcat, NginX etc and some tool chains like Jenkins, TravisCI, CircleCI, GitLab, Dockers and Virtual Machines. 

## DevOps
DevOps is an agile culture. It focuses more on the integration among DEVelopement and OPerationS teams rather than on some process. DevOps tries to cross train development and operations team so that each team can have a clear knowledge how the other team works and thereby increasing mutual understanding and making the interfacing among teams more seamless and hassle free.

## What is Git
Git is a version control system. In simple words, it serves as an automated storage for works which allows creation of multiple branches for a single project, merging works and branches and chronological backtracking and roll back to specific commits (commits are like land marks in work). Git can be used to connect remote repositories like GitHub or BitBucket which allows online storage and integration among multiple users.

## What is Jenkins
Jenkins is an automation service written in Java. It can fetch projects from remote repositories and then perform various operations like building, testing and automated deployment on it. It can perform conditional, multi-branch and parallel tasks. Support of plug-ins make Jenkins really powerful and easy to use. For instance, mail plug-in can easily send mail notifications based on the failure or success or success after consecutive failures of a specific job to specific responsible teams or members. It also supports fully automated and conditional fetching from repositories.