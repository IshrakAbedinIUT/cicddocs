# Week1 Report
------
## Tasks
![Week 1 Tasks](./resources/week1tasks.png "Week1 Tasks")

------
## Task 1: Installing Linux (Debian)
Debian Linux, specially *Ubuntu* is preferred for network server and automation based works. Since I do not like *GNOME* Ubuntu much, I chose *Kubuntu 19.10* as my preferred OS for the tasks which just uses *KDE Plasma* as its default desktop instead of GNOME. [Get Kubuntu from here](https://kubuntu.org/getkubuntu/). However, if you prefer GNOME Ubuntu, [get Ubuntu from here](https://ubuntu.com/#download).

Setting up Linux according to one's own choice is highly recommended since it can provide huge performance boost and better work experience. Also, it is aesthetically pleasing. 

## Task 2: Installing Packages
Some basic packages serve as the dependency prerequisite for other packages. Thankfully most of these comes preloaded with Ubuntu distribution. Rest of the packages will be installed throughout the whole process. For the first week, the aforementioned packages are compulsory. Their installation commands or procedures are given below.

### *gcc*
```bash
$ sudo apt install gcc
```
### *gcc-multilib*
```bash
$ sudo apt install gcc-multilib
```
### *jre8 or jdk8 (8 is better for Jenkins)*
```bash
$ sudo apt install openjdk-8-jre
```
or
```bash
$ sudo apt install openjdk-8-jdk
```
### *dotnet core*
Follow the instructions in their [official website](https://docs.microsoft.com/en-us/dotnet/core/install/linux-package-manager-ubuntu-1904). Choose the latest version of Ubuntu or the distro that is being used.
### *git*
```bash
$ sudo apt install git
```
### *jenkins*
Follow the instructions in this [link](https://linuxize.com/post/how-to-install-jenkins-on-ubuntu-18-04/). It is recommended to download the recommended or most used plug-ins.
### Recommendation
It is recommended to use some kind of editor for editing codes and other scripts. I personally use **Visual Studio Code** or **vscode**. Vscode is self updating and can be downloaded from their [official website](https://code.visualstudio.com/) as package and then installed using the installer.

## Task 3: Making a Basic DotNet Core Application
Though knowledge about writing DotNetCore application is not required for the tasks, it is necessary to know how to create, restore, clean, build and the publish a DotNet Core console app. The commands are as follows:

### Initialization
```bash
dotnet new console
```
### Restoration of packages
```bash
dotnet restore [<ROOT>]
```
### Cleaning up build/solution
```bash
dotnet clean [<ROOT>]
```
### Building the project
```bash
dotnet build [<ROOT>]
```
### Testing of managed codes
```bash
dotnet test [<ROOT>]
```
### Publishing application in dll format
```bash
dotnet publish -c release [-o|--output] 
```
*Only some of the commands with specific instances of the commands are shown. A simple starter tutorial can be found at this [getting started link](https://docs.microsoft.com/en-us/dotnet/core/tutorials/using-with-xplat-cli).*

## Task 4: Making a Basic Jenkins Freestyle Project with the DotNet App
First of all Jenkins needs to be completely setup and the user needs to be configured by getting into Jenkins from a web browser using the predefined or [default Jenkins link at port 8080](http://localhost:8080). After the unlocking and first administrative account setting has been completed, first project can be created by selecting new item and then procedurally creating a *free style* project with a suitable name. The most important part of the free style project is the build command and post build command boxes located at the end of the configuration setups. Just like a shell script, put the step by step build commands there.

**N.B. It is the best practice to clone the repo or create the project files using user jenkins under its workspace. In that case, Jenkins will have full permission to perform CRUD operations on the project. *ENV*ironment* variables might needed to be tweaked depending on the implemented language. Not working as jenkins user arouses permission granting problems and complexity.*

## Task 5: Making a Basic Jenkins Pipeline Project
Jenkins *Pipeline* projects offer better customizability, granularity, encapsulation and view over *freestyle* projects. Also *multi-branch Pipeline* allows to account for multiple branches in remote repository with a single script. Though Pipeline can be used for local projects, it is essentially made to perform ci/cd on projects stored in remote repositories like in *Github* or *Bitbucket* etc. The main difference between Pipeline and freestyle project is that Pipeline projects contain a **Jenkinsfile**. The Jenkinsfile is basically a text file where the CD commands are written using *Pipeline Domain-Specific Language* or *JSL* which is based on *Groovy*. Though the Jenkinsfile can be written inside the configuration of the Jenkins project, the best practice is to provide the file with the project. The file can be linked by mentioning the file path in the project configuration which is by default *"./Jenkinsfile"*. Jenkinsfile supports two syntaxes, *Declarative* and *Scripted*. Declarative Pipeline is a more recent feature of Jenkins Pipeline. A basic Jenkinsfile may look like below:

```groovy
pipeline {
    agent any 
    stages {
        stage('Build') { 
            steps {
                // Build command
            }
        }
        stage('Test') { 
            steps {
                // Test command
            }
        }
        stage('Deploy') { 
            steps {
                // Deploy command
            }
        }
    }
}
```

More about Jenkins Pipeline syntax can be found [here](https://jenkins.io/doc/book/pipeline/).

------
## Reading Topics
------
- Linux basics
    + File System
    + Permissions
    + User
    + Group
    + Packages
- What is CI/CD?
- How does Jenkins work?
- What are the Jenkins features and plug-ins
- Build procedure of DotNet Core apps
- What is API?
- Git commands
- Git
    + Branches
    + Merge
    + Rebase
    + Pull request
- What are Web servers?
    + NginX
    + What is reverse proxy?
------
## Related
------
1. [Sample DotNet project with Jenkins file](https://bitbucket.org/IshrakAbedinIUT/banksolution)
2. CI/CD Stages
![CI/CD Stages](./resources/cicdstages.png "CI/CD Stages")