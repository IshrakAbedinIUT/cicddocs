# Week2 Report: Continuous Deployment and Multi-branch Pipeline
------
## Goal

Our goal is to automatically deploy the application we built previous week from build server to application server. So we need a password less access from build server to application server while maintaining authentication. The best way to do this is to use SSH RSA key encryption. The public key placed in application server removes the necessity of password based authentication. We also need to execute some sudo commands in application server for daemon related tasks. Since the build server cannot enter sudo password into application server, we need to ensure that an authenticated user in application server, that the build server will morph into, can execute sudo commands without password. 

------
## Related Information

### Secure Shell (SSH)

Secure Shell (SSH) is a cryptographic network protocol that is used to remotely control network services securely over an unsecured network. It is generally used in a client server based network. SSH can be used to remotely execute commands, transfer files and perform other network based services. One of the major aspect of SSH is that instead of using plain text based password, it uses RSA 2048 or 4096 encrypted keys, making the communication more secure. The default port for SSH is 22. For our task, we will uses *OpenSSH*. If the system does not come with SSH, *Open SSH* can be installed by typing the suggested command that comes after typing *ssh* in terminal. More about SSH can be found at [Wikipedia](https://en.wikipedia.org/wiki/Secure_Shell) and website of [OpenSSH](https://www.openssh.com/).

### Rsync

Rsync is a remote transferring and synchronization utility. It can synchronize files between different directories of the same host or between remote hosts connected over a network using SSH. The rsync algorithm is *delta encoding* based, meaning it synchronizes files based on the changes. So it prevents redundant copy operations minimizing network resource consumption. Moreover, *Zlib* can be used additionally to compress the files to be sent. More about rsync can be found at [Wikipedia](https://en.wikipedia.org/wiki/Rsync) and website of [rsync](https://rsync.samba.org/).

### Daemons

Daemons are background services. Modern *nix systems uses daemons to take care of background processes as well as handle hardwares, network services and log ins. Most linuxes used to implement their own version of *SysV* *init* daemon before to take care of startup processes and background tasks. But recently, most of them have moved to SystemD, a newer implementation which speeds up the process through parallel processing. SystemD is a super server, meaning it takes care of lesser daemon processes as well as itself. There are other daemons to take care of other tasks, such as NetworkD or LoginD to take care of network services and handling log in. SystemD is the actual daemon that manages and *SystemCtl* is the interface through which users can modify, control and check status of running daemons. We would run our application through a daemon process. It gives us couple of advantages. First of all, it auto handles failures and allows simple hot swap of process decreasing down time many folds. It also allows simple auto startup of application in case of power outage.

------
## Tasks
![Week 2 Tasks](./resources/week2tasks.png "Week2 Tasks")

------
## Task 1: Execute sudo without password

We will treat build server as client and application server as host. Since we will be using SSH to perform the commands, we need the main user of build server that has SSH public key to be able to execute sudo commands without password. We need to modify the */etc/sudoers* file, but doing so manually is unsafe. Rather, we will use the following command.

```bash
$ sudo visudo
```
It will open the file in the default terminal editor such as Vim or Nano and allow us to modify it securely while implementing a lock. We just need to add the following lines.

```
username ALL=(ALL) NOPASSWD:ALL
```

We need to perform this for the main user of application server.

## Task 2: Setup SSH in client (build server) and tranferring public key to host (application server)

Now we need to setup a SSH private key in client server and send its corresponding public key to host server.

### Allowing password based authentication for host (build server)

Since we need to put the client's SSH key in host, for the very first use, we need to do it manually with password. So, host's password based authentication needs to be turned on. It can be done by modifying the */etc/ssh/sshd_config* file. We need to open it in an editor as **_root_**.

```bash
vim /etc/ssh/sshd_config
```
After that, we need to find the field *PasswordAuthentication*. It will either be commented as **# PasswordAuthentication yes** or directly unset as **PasswordAuthentication no**. We just need to change it to 

```bash
PasswordAuthentication yes
```
Finally, we need to restart the server after saving the file by running

```bash
/etc/init.d/sshd restart
```

### Generating SSH key and copying the public key to application server

The following methods are for Ubuntu and they may differ based on version or distro. First of all, in the client (build server) pc, we need to generate an RSA keypair by typing the following command.

```bash
$ ssh-keygen
```

After the command we need to give the confirmations and after that our key is ready. We will be shown a graphical representation of our key. It is a good practice to use a pass phrase for the key. But in our case, we are not going to use one. Now we need to copy the key to the host or application server. It is very easy if we have *ssh-copy-id*. The command is

```bash
$ ssh-copy-id username@remote_host
```
Since we are doing it for the first time, we need to enter the password of the host and after that, our secure SSH connection is ready. 

Additionally, we can turn off host's password based authentication off afterwards that we turned on in the first step.

More information about setting up SSH can be found [here](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804).

## Task 3: Setting up a SystemD service
We are going to use a daemon to run our application. Let us assume, we are running a dotnet core application dll called *demoapp.dll*. We will make a daemon service configuration file called *demoapp.service* under the folder */etc/systemd/system/*. The steps of creation and editing are

```bash
$ sudo touch /etc/systemd/system/demoapp.service
$ vim /etc/systemd/system/demoapp.service
```
The contents of the configuration file are like below

```
[Unit]
Description=demo service
After=network.target

[Service]
ExecStart=/usr/bin/dotnet /home/host/demo/demo.dll
Restart=on-failure

[Install]
WantedBy=multi-user.target 
```
*Description* contains the name of service. *ExecStart* runs a command, here the first one is the path to dotnet SDK and second one is the path to our dll. Restart on failure tells the service to auto restart incase of failure.

Now we need to register our daemon and start it and check its status. The steps are as follows.

```bash
$ sudo systemctl daemon-reload # Reloads the daemons that are under /etc/systemd/system folder
$ sudo systemctl enable demoapp.service # Activates our daemon
$ sudo systemctl start demoapp.service # Starts our service daemon
$ sudo systemctl status demoapp.service # Shows the status of our service
```
Since we still do not have the app, the service will show failure as status. Now just need to setup a Jenkins Pipeline project will appropriate Jenkinsfile.

## Task 4: Preparing Jenkins project and Jenkinsfile

We will setup a simple Jenkins Pipeline project with our remote repository location. The project should contain a Jenkinsfile like below.

```groovy
pipeline{
    agent any
    stages{
        stage('Restore'){
            steps{
                sh "dotnet restore"
            }
        }
        stage('Clean'){
            steps{
                sh "dotnet clean"
            }
        }
        stage('Build'){
            steps{
                sh "dotnet build"
            }
        }
        stage('Publish'){
            steps{
                sh "dotnet publish -c release -o ./Output"
            }
        }
        stage('Deploy'){
            steps{
                sh "rsync -avz ./Output/ username@remote_host:demo/"
                // rsync is used to sync files
                // -a means all files recursively with delta calculations
                // -v means verbose, so that we can see what files are being copied
                // -z means use Zlib to zip the files for efficient resource use
                // ./Output/ is the dll's location in client (build server)
                // username@remote_host:demo/ is the dll location in host (app server) 
            }
        }
        stage('Restart Daemon'){
            steps{
                sh 'ssh username@remote_host:demo "sudo systemctl restart demoapp.service"'
                // SSH is used to remotely restart host's daemon.
            }
        }
    }
}
```

More on rsync can be found [here](https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories-on-a-vps).

## Task 5: Testing the project
If all the steps were performed properly, we will be able to see the status of our application properly by typing, 

```bash
$ sudo systemctl status demoapp.service
```
It will say it is enabled, active and loaded. Additionally we can use *journalctl* if we wish to communicate with the app stdin or stdout directly and monitor it in real time.

## Task 6: Multi-branch Pipeline
Multi-branch Pipeline projects are very similar to normal Pipeline projects, except the fact that they are designed to work with multi-branch remote SCM. It auto scans each of the branches either periodically or based on web hook and if any change is detected, it auto runs Jenkinsfile for the branch. The Jenkins file can distinct and differentiate between branches by using a special block like below.
```groovy
...
stage('stagename'){
    when{
        branch 'branchname'
    }
    steps{
        ...
    }
}
...
```
A sample multi-branch project can be found [here](https://bitbucket.org/IshrakAbedinIUT/netcore-api/). The Jenkinsfile of *dev* branch is shown below.

```groovy
pipeline{
    agent any
    stages{
        stage('Master Build'){
            when {
                branch 'master' 
            }
            steps{
                sh "/home/ishrak/NC2/dotnet restore"
                sh "/home/ishrak/NC2/dotnet clean"
                sh "/home/ishrak/NC2/dotnet build"
                sh "/home/ishrak/NC2/dotnet publish -c release -o ./Output"
            }
        }

        stage('Dev Test and Build'){
            when {
                branch 'dev' 
            }
            steps{
                sh "/home/ishrak/NC2/dotnet restore"
                sh "/home/ishrak/NC2/dotnet clean"
                sh "/home/ishrak/NC2/dotnet build"
                sh "/home/ishrak/NC2/dotnet test --logger 'trx;LogFileName=NetCore.API.trx' -r ./Result/ netcore-api.sln"
                mstest testResultsFile:"**/*.trx", keepLongStdio: true
                sh "/home/ishrak/NC2/dotnet publish -c release -o ./Output"
            }
        }

        stage('Dev Deploy'){
            when {
                branch 'dev' 
            }
            steps{
                sh "rsync -avz ./api/Output/ ishrak@localhost:app/netcoreapidev/"
            }
        }

        stage('Dev Daemon Restart'){
            when {
                branch 'dev' 
            }
            steps{
                sh 'ssh ishrak@localhost "sudo systemctl restart netcoreapidev.service"'
            }
        }
    }
}
```
------
## Reading Topics
------
- SystemD
- Service
- SystemCtl
- SysCtl
- SysV
- InitD
- Shell Scripting
- Jenkins
    + ENV variables
    + User Management
    + Plugins - Files, SSH, Build, BlueOcean
    + Global Tool Configuration
    + Multibranch Pipeline
------